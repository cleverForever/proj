import os
from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'converter.settings')

app = Celery('converter')
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'send-report-every-day': {
        'task': 'Сущности.tasks.send_every_month',
        'schedule': crontab(hour='6', minute='0'),  # change to `crontab(minute=0, hour=0)` if you want it to run daily at midnight
    },
}
