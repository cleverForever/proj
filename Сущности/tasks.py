from django.core.mail import send_mail
from converter.celery import app
from .sender import send
from .models import Subscriptions
import datetime


@app.task
def send_email(user_email, first_name, username):
    send(user_email, first_name, username)


@app.task
def send_every_month():
    subs = Subscriptions.objects.all()
    today = datetime.datetime.now()
    tomorrow = today + datetime.timedelta(days=1)
    for sub in subs:
        if (sub.date.day == tomorrow.day) and \
                (sub.date.month == tomorrow.month) and \
                (sub.date.year == tomorrow.year):
            send_mail(
                'Завтра оплата подписки ' + sub.name,
                'SubHub напоминает вам, что завтра оплаты подписки ' + sub.name,
                'ttesterov20@gmail.com',
                [sub.user.email],
                fail_silently=False,  # При False send_mail вызовет smtplib.SMTPException
            )
        elif (sub.date.day == today.day) and \
                (sub.date.month == today.month) and \
                (sub.date.year == today.year):
            send_mail(
                'Сегодня оплата подписки ' + sub.name,
                'SubHub напоминает вам, что сегодня оплаты подписки ' + sub.name,
                'ttesterov20@gmail.com',
                [sub.user.email],
                fail_silently=False,  # При False send_mail вызовет smtplib.SMTPException
            )
