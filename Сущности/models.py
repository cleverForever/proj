from django.db import models
from time import time
from django.contrib.auth.models import User


def gen_slug():  # генерация слага
    return 'subs' + '-' + str(int(time()))


class Manager(models.Model):
    passport = models.TextField(verbose_name='Паспорт')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    salary = models.CharField(verbose_name='Зарплата', max_length=50)

    class Meta:
        verbose_name = 'Менеджер'
        verbose_name_plural = 'Менеджеры'

    def __str__(self):
        return self.passport


class Tourist(models.Model):
    passport = models.CharField(verbose_name='Паспорт', max_length=20)
    FIO = models.CharField(verbose_name='ФИО', max_length=100)
    location = models.CharField(verbose_name='Место регистрации', max_length=200)
    phone = models.CharField(verbose_name='Телефон', max_length=11)

    class Meta:
        verbose_name = 'Турист'
        verbose_name_plural = 'Туристы'

    def __str__(self):
        return self.FIO


class WithTourist(models.Model):
    passport = models.CharField(verbose_name='Паспорт', max_length=20)
    FIO = models.CharField(verbose_name='ФИО', max_length=100, null=True, blank=True)
    location = models.CharField(verbose_name='Место регистрации', max_length=200, null=True, blank=True)
    phone = models.CharField(verbose_name='Телефон', max_length=11, null=True, blank=True)

    none = 'none'
    father = 'Father'
    mother = 'Mother'
    sister = 'Sister'
    brother = 'Brother'
    wife = 'Wife'
    husband = 'Husband'
    douther = 'Douther'
    son = 'Son'

    seq = (
        (none, '--------'),
        (father, 'Отец'),
        (mother, 'Мать'),
        (sister, 'Сестра'),
        (brother, 'Брат'),
        (wife, 'Жена'),
        (husband, 'Муж'),
        (douther, 'Дочь'),
        (son, 'Сын'),
    )
    relation = models.CharField(verbose_name='Отношение', max_length=10, choices=seq, default=none)

    class Meta:
        verbose_name = 'Попутчик'
        verbose_name_plural = 'Попутчики'

    def __str__(self):
        return self.FIO


class PriceList(models.Model):
    name = models.CharField(verbose_name='Название', max_length=20)
    otkuda = models.DateField(verbose_name='ОТ', )
    kuda = models.DateField(verbose_name='ДО', )

    class Meta:
        verbose_name = 'Прейскурант'
        verbose_name_plural = 'Прейскурант'

    def __str__(self):
        return self.name


class TypeTour(models.Model):
    name = models.CharField(verbose_name='Название', max_length=20)

    class Meta:
        verbose_name = 'Тип тура'
        verbose_name_plural = 'Тип тура'

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(verbose_name='Название', max_length=20)

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'

    def __str__(self):
        return self.name


class Tour(models.Model):
    country = models.ForeignKey(Country, related_name='countries', on_delete=models.CASCADE)
    type = models.ForeignKey(TypeTour, related_name='types', on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название', max_length=35)

    class Meta:
        verbose_name = 'Тур'
        verbose_name_plural = 'Туры'

    def __str__(self):
        return self.name


class Service(models.Model):
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    name = models.CharField(verbose_name='Название', max_length=35)

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return self.name


class StrPriceList(models.Model):
    price_list = models.ForeignKey(PriceList, on_delete=models.CASCADE)
    price = models.CharField(verbose_name='Цена', max_length=5)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Строка прейскуранта'
        verbose_name_plural = 'Строки прейскуранта'

    def __str__(self):
        return self.price


class TravelPackage(models.Model):
    manager = models.ForeignKey(Manager, on_delete=models.CASCADE)
    tour = models.ForeignKey(Tour, on_delete=models.CASCADE)
    tourist = models.ForeignKey(Tourist, on_delete=models.CASCADE)
    with_tourist = models.ForeignKey(WithTourist, on_delete=models.CASCADE, null=True, blank=True)
    start = models.DateField(verbose_name='Дата отправления', )
    end = models.DateField(verbose_name='Дата прибытия', null=True, blank=True)
    location_from = models.CharField(verbose_name='Место отправления', max_length=100)
    location_to = models.CharField(verbose_name='Место прибытия', max_length=100)

    class Meta:
        verbose_name = 'Путёвка'
        verbose_name_plural = 'Путёвки'
