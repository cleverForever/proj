from django.core.mail import send_mail


def send(user_email, first_name, username):
    message = 'Данные вашего профиля были изменены.\nЕсли это бы не вы, смените пароль\n' + \
              'Логин: ' + username + '\n' + \
              'Имя пользователя: ' + first_name + '\n' + \
              'Email: ' + user_email
    send_mail(
        'Изменение данных профиля',
        message,
        'ttesterov20@gmail.com',
        [user_email],
        fail_silently=False,  # При False send_mail вызовет smtplib.SMTPException
    )
