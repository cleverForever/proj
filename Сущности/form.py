from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import DateTimeInput, DateInput

from Сущности.models import Tourist, WithTourist, Tour, TravelPackage


class TouristForm(forms.ModelForm):
    FIO = forms.CharField(label='ФИО', required=True)
    location = forms.CharField(label='Место регистрации', required=True)
    phone = forms.CharField(label='Телефон', required=True)

    class Meta:
        model = Tourist
        fields = ['FIO', 'passport', 'location', 'phone']


class WithTouristForm(forms.ModelForm):
    class Meta:
        model = WithTourist
        fields = ['FIO', 'passport', 'location', 'phone', 'relation']


class TravelPackageForm(forms.ModelForm):
    start = forms.DateField(
        label='Дата отправления',
        required=True,
        widget=DateInput(attrs={'type': 'date'}),
        localize=True
    )
    end = forms.DateField(
        label='Дата прибытия',
        required=True,
        widget=DateInput(attrs={'type': 'date'}),
        localize=True
    )

    class Meta:
        model = TravelPackage
        fields = ['tour', 'start', 'end', 'location_from', 'location_to']

    def __init__(self, manager, tourist, *args, **kwargs):
        super(TravelPackageForm, self).__init__(*args, **kwargs)
        self.instance.manager_id = manager.id
        self.instance.tourist_id = tourist.id
