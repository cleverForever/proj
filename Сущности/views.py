from django.contrib.auth import logout, login, get_user
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import CreateView
from django.views.generic.base import View

# Список всех подписок
from Сущности.form import TouristForm, WithTouristForm, TravelPackageForm
from Сущности.models import Manager, TravelPackage, Tourist


class Profile(View):
    def get(self, request):
        if request.user.is_authenticated:
            packages = TravelPackage.objects.filter(manager=request.user.pk)
            context = {
                'name': request.user.username,
                'packages': list(packages)
            }
            return render(request, 'Сущности/profile.html', context)
        else:
            return render(request, 'Сущности/info_anonymous.html')


class CreateTouristView(CreateView):
    template_name = 'Сущности/tourist/create_tourist.html'
    form_class = TouristForm

    def get_success_url(self):
        return reverse('create_travel_package')


class CreateWithTouristView(CreateView):
    template_name = 'Сущности/tourist/create_with_tourist.html'
    form_class = WithTouristForm

    def get_success_url(self):
        return reverse('profile')


class CreateTravelPackageView(CreateView):
    template_name = 'Сущности/travel_package/travel_package.html'
    form_class = TravelPackageForm

    def get_success_url(self):
        return reverse('profile')

    def get_form_kwargs(self):
        kwargs = super(CreateTravelPackageView, self).get_form_kwargs()
        kwargs['manager'] = get_user(self.request)
        kwargs['tourist'] = list(Tourist.objects.all())[-1]
        return kwargs


class InfoView(View):
    def get(self, request):
        return render(request, 'Сущности/info_anonymous.html')
