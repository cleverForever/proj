from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('', views.InfoView.as_view(), name='info'),
    path('create_tourist/', views.CreateTouristView.as_view(), name='create'),
    # path('create_with/', views.CreateWithTouristView.as_view(), name='create_with'),
    path('create_travel_package/', views.CreateTravelPackageView.as_view(), name='create_travel_package'),
    path('profile/', views.Profile.as_view(), name='profile'),
    path('login/', LoginView.as_view(), name='login'),
    path('login/', LogoutView.as_view(), name='logout'),
]
