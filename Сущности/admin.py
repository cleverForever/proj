from django.contrib import admin


# Register your models here.
from Сущности.models import Manager, Tourist, WithTourist, PriceList, TypeTour, Country, Tour, Service, \
    StrPriceList, TravelPackage

admin.site.register(Manager)
admin.site.register(Tourist)
admin.site.register(WithTourist)
admin.site.register(PriceList)
admin.site.register(TypeTour)
admin.site.register(Country)
admin.site.register(Tour)
admin.site.register(Service)
admin.site.register(StrPriceList)
admin.site.register(TravelPackage)